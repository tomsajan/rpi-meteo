from prometheus_client import start_http_server, Summary, Counter, Gauge
from arduino.arduino import Arduino
from main import (
    # get_dht11_data,
    get_dht_adafruit_data,
    get_photoresistor_data,
    get_arduino_data)

import time


class MeteoScraper:
    def __init__(self):

        # arduino needs to keep connection open
        # (in comparison to the other sensors)
        self.arduino = Arduino()

        # Create a metric to track time spent and requests made.
        self.scrape_time = Summary('rpi_meteo_scraping_seconds',
                                   'Time spent scraping sensors')

        self.pir = Counter('rpi_meteo_pir', 'Movement count recorded by PIR')
        self.sound = Counter('rpi_meteo_sound', 'Noise above threshold count')

        self.temp = Gauge('rpi_meteo_temp', 'Temperature')
        self.hum = Gauge('rpi_meteo_humidity', 'Humidity')
        self.light = Gauge('rpi_meteo_light', 'Level of light',
                           ['sensor'])

    def close(self):
        self.arduino.close_serial()

    def scrape(self):
        with self.scrape_time.time():

            # dht = get_dht11_data()
            dht = get_dht_adafruit_data(22)
            light_rpi = get_photoresistor_data()
            ard = get_arduino_data(self.arduino)

            if dht['humidity']:
                self.hum.set(float(dht['humidity']))
            if dht['temperature']:
                self.temp.set(float(dht['temperature']))
            if light_rpi['light_rpi']:
                self.light.labels(sensor='rpi').set(int(light_rpi['light_rpi']))
            if ard.get('light_ard', None):
                self.light.labels(sensor='arduino').set(int(ard['light_ard']))
            if ard.get('pir', None):
                self.pir.inc(int(ard['pir']))
            if ard.get('sound', None):
                self.sound.inc(int(ard['sound']))

            # self.pir.inc(2)
            # self.sound.inc(3)
            # self.temp.set(25)
            # self.hum.set(50)
            # self.light.labels(sensor='rpi').set(500)
            # self.light.labels(sensor='arduino').set(128)


if __name__ == '__main__':
    # Start up the server to expose the metrics.
    start_http_server(9088)

    t0 = time.time() - 60
    t1 = t0 + 60
    ms = MeteoScraper()
    try:
        while True:
            t1 = time.time()
            if t1 - t0 > 60:
                ms.scrape()
                t0 = t1
            else:
                time.sleep(5)
    except KeyboardInterrupt:
        ms.close()
