from sensors.photoresistor import Photoresistor
from sensors.dht11 import DHT
from sensors.dht_ada import DHTAda
from utils.timeout import Timeout
import requests
import configparser
import time
import os

script_dir = os.path.dirname(__file__)
config = configparser.ConfigParser()
cfg_file = open(os.path.join(script_dir, 'meteo.conf'), 'r')
config.read_file(cfg_file)


def average(values):
    return round(sum(values)/float(len(values))) if len(values) > 0 else None


def rename(where, old, new):
    where[new] = where.pop(old)


def to_lower(d):
    for k in d.keys():
        d[k.lower()] = d.pop(k)


def get_photoresistor_data():
    pr = Photoresistor()
    values = list()
    try:
        for _ in range(3):
            with Timeout(config.getint('VALUES', 'timeout')):
                values.append(pr.get_data())
            time.sleep(0.5)
        return {'light_rpi': average(values)}
    except:
        return {'light_rpi': None}


def get_dht_adafruit_data(model):
    dht = DHTAda(model)
    humidity, temperature = dht.get_data()
    return {'humidity': humidity, 'temperature': temperature}


def get_dht11_data():
    dht = DHT()
    values_hum = list()
    values_temp = list()
    try:
        dht.get_data()  # the first measurement is different from the others
    except:
        pass

    for _ in range(3):
        time.sleep(1)
        try:
            value = dht.get_data()
            values_hum.append(value[0])
            values_temp.append(value[1])
        except:
            pass

    avg_hum = average(values_hum)
    avg_temp = average(values_temp)
    return {'humidity': avg_hum, 'temperature': avg_temp}


def get_arduino_data(arduino):
    for i in range(3):
        try:
            data = arduino.read_arduino()
            if 'LIGHT' in data:
                rename(data, 'LIGHT', 'light_ard')
            if 'PIR' in data:
                rename(data, 'PIR', 'pir')
            if 'SOUND' in data:
                rename(data, 'SOUND', 'sound')
            print(data)
            return data
        except Exception as e:
            print('Arduino: {}'.format(e))
            pass
    # arduino.close_serial()
    return dict()


def send_data(**kwargs):
    r = requests.get(config.get('REMOTE', 'remote_url'), params=kwargs)
    r.raise_for_status()



