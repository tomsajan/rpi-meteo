function doGet(e) {
//
// var temperature = parseInt(e.parameter.temperature);
// var humidity = parseInt(e.parameter.humidity);
// var light = parseInt(e.parameter.light);

 var temp = "";
 var hum = "";
 var light = "";
  if ('temperature' in e.parameter) {
   temp =  parseInt(e.parameter.temperature);
  }
  if ('humidity' in e.parameter) {
   hum =  parseInt(e.parameter.humidity);
  }
  if ('light' in e.parameter) {
   light =  parseInt(e.parameter.light);
  }
  
 var dt = Utilities.formatDate(new Date(), "Europe/Prague", "yyyy/MM/dd HH:mm");

 save_data(temp, hum, light, dt);
 save_to_fusion(temp, hum, light, dt);
 Logger.log(e.parameter);

}

function save_data(temp, hum, light, dt){
  var doc = SpreadsheetApp.openById("18Gm7P0mNh1EIrTAYLwe-vlmXvrjcyLQThtY6s_eobf8");
  var list = doc.getSheets()[0];
  list.deleteRow(2);

  var range = list.getRange("A97:D97");
//  var dt = Utilities.formatDate(new Date(), "Europe/Prague", "yyyy/MM/dd HH:mm");
  range.setValues([[dt, temp, hum, light]]);
}

function save_to_fusion(temp, hum, light, dt) {
  var ft_id = '10WmBs2xQ-umWl44XNxz0ZHHdfs0bFm4eJiEn3-NR';
//  var dt = Utilities.formatDate(new Date(), "Europe/Prague", "yyyy/MM/dd HH:mm");
  FusionTables.Query.sql("INSERT INTO " + ft_id +
                         "(datetime, Temp, Hum, Light) VALUES (" +
                         "'" + dt + "', '" + temp  + "', '" + hum + "', '" + light + "');"
                        );

}


//}

/**
 * Created by janik on 6.6.17.
 */
