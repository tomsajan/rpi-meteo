import glob
import serial
import re

DATA_RE = re.compile(r'(?:(?P<key>[A-Z]+):(?P<val>\d+);)')
CRC_RE = re.compile(r'.*CRC:(?P<crc>\d+)$')


class Arduino:
    def __init__(self):
        self.serial = None
        self.open_serial()

    @staticmethod
    def process_data(raw_data):
        """
        processes raw data from arduino into dict
        checks CRC

        example data: SOUND:0;LIGHT:778;PIR:9;CRC:787
        :param raw_data:
        :return:
        """
        raw_data = raw_data.strip()
        data = dict(DATA_RE.findall(raw_data))
        crc = int(CRC_RE.findall(raw_data)[0])

        if sum(map(int, data.values())) != crc:
            raise ValueError('CRC sum does not match data')

        return data

    @staticmethod
    def get_tty():
        ttys = glob.glob('/dev/ttyUSB*')
        if len(ttys) == 0:
            raise Exception('No serial console found. Cannot read Arduino')
        return ttys[0]

    def open_serial(self):
        if not self.serial or not self.serial.is_open:
            self.serial = serial.Serial(self.get_tty(), 9600, timeout=1,
                                        write_timeout=1)

    def read_arduino(self):
        self.open_serial()

        self.serial.reset_input_buffer()
        self.serial.reset_output_buffer()
        self.serial.write('GET_DATA\n'.encode('ascii'))
        self.serial.flush()
        raw_data = self.serial.readline().decode('ascii')
        try:
            data = self.process_data(raw_data)
        except Exception as e:
            print(e)
            self.serial.write('CONTINUE\n'.encode('ascii'))
            self.serial.flush()
            raise
        else:
            self.serial.write('RESET\n'.encode('ascii'))
            self.serial.flush()

        return data

    def close_serial(self):
        self.serial.close()

    def reset_serial(self):
        self.close_serial()
        self.open_serial()
