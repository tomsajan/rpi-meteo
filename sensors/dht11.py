from .sensor import Sensor
import time
import RPi.GPIO as GPIO

DATA_PIN = 18
# VCC_PIN = 15

HIGH_ZERO_FROM = 2
HIGH_ZERO_TO = 11
HIGH_ONE_FROM = 12
HIGH_ONE_TO = 30
BUFFER_SIZE = 3000


def bin2dec(string_num):
    return str(int(string_num, 2))


class DHT(Sensor):
    def setup(self):
        GPIO.setmode(GPIO.BCM)

    def run(self):
        # measurement_init()
        return self.take_data()

    def cleanup(self):
        GPIO.cleanup()

    def take_data(self):
        # Inform the sensor to start measurement
        GPIO.setup(DATA_PIN, GPIO.OUT)
        GPIO.output(DATA_PIN, GPIO.HIGH)
        time.sleep(0.025)
        GPIO.output(DATA_PIN, GPIO.LOW)
        time.sleep(0.02)

        # read data
        GPIO.setup(DATA_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        data = []
        for i in range(0, BUFFER_SIZE):
            data.append(GPIO.input(DATA_PIN))

        count = 0
        BITS = ""
        try:
            while data[count] == 0:
                count += 1
            while data[count] == 1:
                count += 1

            for bit in range(0, 40):
                bit_count = 0

                while data[count] == 0:
                    count += 1

                while data[count] == 1:
                    bit_count += 1
                    count += 1
                if HIGH_ZERO_FROM <= bit_count <= HIGH_ZERO_TO:
                    BITS += "0"
                elif HIGH_ONE_FROM <= bit_count <= HIGH_ONE_TO:
                    BITS += "1"
                else:
                    raise Exception("Signal out of the range")

        except:
            print("ERR_RANGE")
            raise

        if len(BITS) != 40:
            raise Exception("Not enought data")
        HID = bin2dec(BITS[0:8])    # humidity
        HDD = bin2dec(BITS[8:16])
        TID = bin2dec(BITS[16:24])  # temperature
        TDD = bin2dec(BITS[24:32])
        CRC = bin2dec(BITS[32:40])  # checksum

        # print(HID, HDD)
        # print(TID, TDD)
        # print(CRC)
        # print(int(HID) + int(HDD) + int(TID) + int(TDD) - int(CRC))

        if (int(HID) + int(HDD) + int(TID) + int(TDD) - int(CRC)) == 0:
            return int(HID), int(TID)
        else:
            raise Exception("Checksum failed")

    def get_data(self):
        self.setup()
        try:
            HUM, TEMP = self.run()
            print(HUM, TEMP)
            return HUM, TEMP
        except Exception as e:
            print(e)
            raise

        finally:
            self.cleanup()
