import Adafruit_DHT

DATA_PIN = 18

# https://pimylifeup.com/raspberry-pi-humidity-sensor-dht22/


class DHTAda:
    def __init__(self, model=22):
        if model == 22:
            self.sensor = Adafruit_DHT.DHT22
        elif model == 11:
            self.sensor = Adafruit_DHT.DHT11
        else:
            raise ValueError('Unsupported DHT model {}'.format(model))

    def get_data(self):
        try:
            humidity, temperature = Adafruit_DHT.read_retry(self.sensor,
                                                            DATA_PIN,
                                                            retries=6,
                                                            delay_seconds=3)
            print(humidity, temperature)
            if humidity is None or temperature is None:
                # raise Exception('DHT measurement failed')
                raise Exception('DHT measurement failed')
            return round(humidity, 2), round(temperature, 2)
        except Exception as e:
            print(e)
            return None, None
