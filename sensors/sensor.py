from abc import ABCMeta, abstractmethod, abstractclassmethod


class Sensor(metaclass=ABCMeta):

    @abstractmethod
    def setup(self):
        pass

    @abstractmethod
    def run(self):
        pass

    @abstractmethod
    def cleanup(self):
        pass

    @abstractmethod
    def get_data(self):
        pass
