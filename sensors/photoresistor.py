from .sensor import Sensor
import time
import RPi.GPIO as GPIO

DATA_PIN = 17
# VCC_PIN = 27
DISCHARGE_TIME = 1  # in seconds


class Photoresistor(Sensor):

    def setup(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(DATA_PIN, GPIO.OUT, initial=GPIO.LOW)

    def measurement_init(self):
        GPIO.setup(DATA_PIN, GPIO.OUT)
        GPIO.output(DATA_PIN, GPIO.LOW)
        time.sleep(DISCHARGE_TIME)

    def take_data(self):
        GPIO.setup(DATA_PIN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

        val = 0
        while GPIO.input(DATA_PIN) == GPIO.LOW:
            val += 1
        print(val)
        return val

    def run(self):
        self.measurement_init()
        return self.take_data()

    def cleanup(self):
        GPIO.cleanup()

    def get_data(self):
        self.setup()
        try:
            return self.run()
        except Exception as e:
            print(e)
            raise
        finally:
            self.cleanup()

