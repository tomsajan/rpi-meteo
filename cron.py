#!/usr/bin/env python3
from main import (
    get_dht11_data,
    get_photoresistor_data,
    send_data,
    get_arduino_data,
    to_lower,
)


if __name__ == '__main__':
    data = {}
    data.update(get_dht11_data())
    data.update(get_photoresistor_data())
    data.update(get_arduino_data())

    to_lower(data)
    print(data)
    send_data(**data)
